console.log("Script is running");
let x;
let y = new Object();
Object.getPrototypeOf(x);
Object.getPrototypeOf(y);
let math = <h1>2+3=2+3</h1>;
ReactDOM.render(math, document.getElementById('app'));

var access = (age >= 21) ? "yes" : "no";
var psmsg = (psswd = true) ? "Access Granted" : "Permission Denied";

// -----------TOIN COSS FOR A PICTURE ----------------/
import React from 'react';
import ReactDOM from 'react-dom';
function coinToss() {
    // This function will randomly return either 'heads' or 'tails'.
    return Math.random() < 0.5 ? 'heads' : 'tails';
}
const pics = {
    kitty: 'https://s3.amazonaws.com/codecademy-content/courses/React/react_photo-kitty.jpg',
    doggy: 'https://s3.amazonaws.com/codecademy-content/courses/React/react_photo-puppy.jpeg'
};
let img;

// if/else statement begins here:
if (coinToss() === 'heads') {
    img = (
        <img src={pics.kitty} />
    )
} else {
    img = <img src={pics.doggy} />;
}
ReactDOM.render(img, document.getElementById('app'));

// --Same as above but with a teneray operator
import React from 'react';
import ReactDOM from 'react-dom';

function coinToss() {
    // Randomly return either 'heads' or 'tails'.
    return Math.random() < 0.5 ? 'heads' : 'tails';
}

// ------Random pick a pic -------/
const pics = {
    kitty: 'https://s3.amazonaws.com/codecademy-content/courses/React/react_photo-kitty.jpg',
    doggy: 'https://s3.amazonaws.com/codecademy-content/courses/React/react_photo-puppy.jpeg'
};
const img = <img src={pics[coinToss() === 'heads' ? 'kitty' : 'doggy']} />;
ReactDOM.render(
    img,
    document.getElementById('app')
);

// ---------Next lesson ------------/
const people = ['Rowe', 'Prevost', 'Gare'];
const peopleLis = people.map(x =>
    // expression goes here:
    <li>{x}</li>
);
ReactDOM.render(
    <ul>{peopleLis}</ul>, document.getElementById('app')
);

//------MAP quickie
let ages = [46, 45, 21, 17, 10];
let place = document.getElementById(app);
place.innerHTML = ages.map(Math.sqrt);

//-----MAP'd index for key
const people = ['Rowe', 'Prevost', 'Gare'];
const peopleLis = people.map((person, i) =>
    // expression goes here:
    <li key={'person_' + i}>{person}</li>
);
ReactDOM.render(
    <ul>{peopleLis}</ul>, document.getElementById('app')
);

//-------Authorization Form with REACT ----/
class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      password: 'swordfish',
      authorized: false
    };
    this.authorize = this.authorize.bind(this);
  }

  authorize(e) {
    const password = e.target.querySelector(
      'input[type="password"]').value;
    const auth = password == this.state.password;
    this.setState({
      authorized: auth
    });
  }

  render() {
    const login = (
    	<form action='#' onSubmit={this.authorize}>
      	<input type='password' placeholder='password'/>
        <input type='submit'/>
      </form>
    );
    const contactInfo = (
     <ul>
          <li>
            client@example.com
          </li>
          <li>
            555.555.5555
          </li>
        </ul>
    );
    return (
      <div id="authorization">
        <h1>
      {this.state.authorized ? 'Contact' : 'Enter the Password'}
       </h1>
			{this.state.authorized ? contactInfo : login }
      </div>
    );
  }
}

ReactDOM.render(
  <Contact />, 
  document.getElementById('app')
);
